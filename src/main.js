import './assets/styles/main.scss';

import Vue from 'vue';
import router from './routes.js';
import store from './store.js';
import Auth from './plugins/Auth.js';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate';

import App from './components/App.vue';

Vue.use(VueAxios, axios);
Vue.use(VeeValidate);

Vue.axios.defaults.baseURL = "http://localhost:3000/api/v1";

// Set the JWT token if it exists
Vue.axios.interceptors.request.use((config) =>{
	if (Vue.auth.getToken()){
    	config.headers['Authorization'] = "Bearer " + Vue.auth.getToken();
	}
	return config;
});

Vue.use(Auth);

router.beforeEach(function(to, from, next){
  if(to.matched.some(function(record){ return record.meta.requiresAuth }) && !Vue.auth.loggedIn()){
    next({
      path: "/?action=login"
    })
  }
  else {
    next();
  }
});

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
