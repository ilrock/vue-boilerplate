import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/pages/Home.vue'
import About from './components/pages/About.vue'

Vue.use(Router)

export default new Router({
	mode: 'history',
  	routes: [
  		{
  			path: "/",
  			component: Home
		},
		{
			path: "/about",
			component: About,
		}
	]
});
